#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <printf.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <mqueue.h>
#include <semaphore.h>
#include <sys/mman.h>

#include "gfserver.h"
#include "cache-student.h"
#include "shm_channel.h"

// Thread safe queue global variables
pthread_mutex_t mutex_shm_free_segments;
pthread_cond_t cond_shm_free_segments;

// Command Channel to the Cache
mqd_t mqd;

// Shared Memory attributes
unsigned int sm_num_segments;
size_t sm_data_size, sm_meta_size;

// Queue of allocated pointers for Shared Memory
steque_t *segments_queue;

void errExit(const char *msg) {
    perror(msg);
    exit(SERVER_FAILURE);
}

void cleanup_server_request(segment_queue_item *sqi, shared_mem_metadata *addr_metadata) {
    sem_destroy(&addr_metadata->sem_file_request);
    sem_destroy(&addr_metadata->sem_file_ready);

    pthread_mutex_lock(&mutex_shm_free_segments);

    steque_enqueue(segments_queue, sqi);

    pthread_mutex_unlock(&mutex_shm_free_segments);
    pthread_cond_broadcast(&cond_shm_free_segments);
}

void cleanup_shared_memory() {
    if (segments_queue != NULL) {
        while(!steque_isempty(segments_queue)) {
            segment_queue_item *sqi = steque_pop(segments_queue);
            free(sqi->data_name);
            free(sqi->meta_name);
            free(sqi);
        }
        steque_destroy(segments_queue);
        free(segments_queue);
    }

    // TODO increase this 256?
    char shm_path[256];
    for (int i = 0; i < sm_num_segments; i++) {
        sprintf(shm_path, SM_DATA_PATH, i);
        if (shm_unlink(shm_path) == -1) {
            perror("cleanup_shared_memory: shm_unlink data_path");
        }
        sprintf(shm_path, SM_META_PATH, i);
        if (shm_unlink(shm_path) == -1) {
            perror("cleanup_shared_memory: shm_unlink meta_path");
        }
    }

    close(mqd);
    if (mq_unlink(MQ_PATH) == -1) {
        perror("cleanup_shared_memory: mq_unlink");
    }

    pthread_mutex_destroy(&mutex_shm_free_segments);
    pthread_cond_destroy(&cond_shm_free_segments);
}

void initialize_shared_memory(unsigned int nsegments, size_t segsize) {
    // Initialize global variables
    sm_num_segments = nsegments;
    sm_data_size = segsize;
    sm_meta_size = sizeof(shared_mem_metadata);

    // Initialize mutexes for access to queue of available shared memory segments
    pthread_mutex_init(&mutex_shm_free_segments, NULL);
    pthread_cond_init(&cond_shm_free_segments, NULL);

    // https://www.softprayog.in/programming/interprocess-communication-using-posix-message-queues-in-linux
    // LPI book below
    struct mq_attr attr, *attrp;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = 2048;
    attrp = &attr;
    int flags = (O_CREAT | O_EXCL | O_WRONLY);
    mode_t perms = (S_IRUSR | S_IWUSR);

    // Create the message queue for file requests to the cache
    mqd = mq_open(MQ_PATH, flags, perms, attrp);
    if (mqd == (mqd_t) -1) {
        errExit("initialize_shared_memory: mqd");
    }

    segments_queue = calloc(1, sizeof(steque_t));
    steque_init(segments_queue);

    // http://pubs.opengroup.org/onlinepubs/009604599/functions/shm_open.html
    int fd;
    void *addr_data;
    for (int i = 0; i < nsegments; i++) {
        segment_queue_item *new_segment = calloc(1, sizeof(segment_queue_item));

        // Create Data Segment
        char sm_data_name[256];
        sprintf(sm_data_name, SM_DATA_PATH, i);
        // Exclusively create shared memory with RW access
        fd = shm_open(sm_data_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
        if (fd == -1) {
            errExit("initialize_shared_memory: shm_open data");
        }
        if (ftruncate(fd, sm_data_size) == -1) {
            errExit("initialize_shared_memory: ftruncate data");
        }
        addr_data = mmap(NULL, sm_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr_data == MAP_FAILED) {
            errExit("initialize_shared_memory: mmap data");
        }
        if (close(fd) == -1) {
            errExit("initialize_shared_memory: close fd data");
        }
        new_segment->data_name = calloc(strlen(sm_data_name) + 1, sizeof(char));
        strcpy(new_segment->data_name, sm_data_name);

        // Create Meta Segment
        char sm_meta_name[256];
        sprintf(sm_meta_name, SM_META_PATH, i);
        // Exclusively create shared memory with RW access
        fd = shm_open(sm_meta_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
        if (fd == -1) {
            errExit("initialize_shared_memory: shm_open meta");
        }
        if (ftruncate(fd, sm_meta_size) == -1) {
            errExit("initialize_shared_memory: ftruncate data");
        }
        addr_data = mmap(NULL, sm_meta_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr_data == MAP_FAILED) {
            errExit("initialize_shared_memory: mmap meta");
        }
        if (close(fd) == -1) {
            errExit("initialize_shared_memory: close fd meta");
        }
        new_segment->meta_name = calloc(strlen(sm_meta_name) + 1, sizeof(char));
        strcpy(new_segment->meta_name, sm_meta_name);

        steque_enqueue(segments_queue, new_segment);
    }
}

ssize_t handle_with_cache(gfcontext_t *ctx, char *path, void* arg) {
    pthread_mutex_lock(&mutex_shm_free_segments);

    // Block until there are available shared memory segments to use
    while (steque_isempty(segments_queue)) {
        pthread_cond_wait(&cond_shm_free_segments, &mutex_shm_free_segments);
    }

    segment_queue_item *sqi = steque_pop(segments_queue);
    pthread_mutex_unlock(&mutex_shm_free_segments);

    // LPI - Mode should be zero if we are not creating the object.
    int fd = shm_open(sqi->meta_name, O_RDWR, 0);
    if (fd == -1) {
        errExit("handle_with_cache: shm_open meta");
    }

    shared_mem_metadata *addr_metadata;
    addr_metadata = mmap(NULL, sm_meta_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr_metadata == MAP_FAILED) {
        errExit("handle_with_cache: mmap meta");
    }

    // LPI - Once we have mapped the object, we can close the file descriptor without
    // affecting the (memory) mapping.
    if (close(fd) == -1) {
        errExit("handle_with_cache: fd close");
    }

    // Initialize Unnamed Semaphore
    // Semaphore is shared among processes so Pshared is non-zero
    int pshared = 1;
    unsigned int initial_value = 0;
    if (sem_init(&addr_metadata->sem_file_request, pshared, initial_value) == -1) {
        errExit("handle_with_cache: sem_file_request init");
    }
    if (sem_init(&addr_metadata->sem_file_ready, pshared, initial_value) == -1) {
        errExit("handle_with_cache: sem_file_ready init");
    }

    // Create file request message to the cache
    char cache_file_request[MAX_CACHE_REQUEST_LEN];
    sprintf(cache_file_request, "%zu %s %s %s", sm_data_size,
            sqi->data_name, sqi->meta_name, path);

    // Send file request using the message queue
    if (mq_send(mqd, cache_file_request, strlen(cache_file_request), 0) == -1) {
        errExit("handle_with_cache: mq_send");
    }

    if (sem_wait(&addr_metadata->sem_file_ready) == -1) {
        errExit("handle_with_cache: sem_file_ready wait");
    }

    // If the file path is not found in the cache
    if (addr_metadata->file_len <= 0) {
        cleanup_server_request(sqi, addr_metadata);
        return gfs_sendheader(ctx, GF_FILE_NOT_FOUND, 0);
    }

    gfs_sendheader(ctx, GF_OK, addr_metadata->file_len);

    // LPI - Mode should be zero if we are not creating the object.
    fd = shm_open(sqi->data_name, O_RDWR, 0);
    if (fd == -1) {
        errExit("handle_with_cache: shm_open");
    }

    shared_mem_data *shm_data_segment;
    shm_data_segment = mmap(NULL, sm_data_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shm_data_segment == MAP_FAILED) {
        errExit("handle_with_cache: mmap data");
    }

    // LPI - Once we have mapped the object, we can close the file descriptor without
    // affecting the (memory) mapping.
    if (close(fd) == -1) {
        errExit("handle_with_cache: fd close");
    }

    size_t bytes_transferred = 0;
    ssize_t write_len;
    char buffer[sm_data_size];
    while (bytes_transferred < addr_metadata->file_len) {
        memset(buffer, 0, sm_data_size);

        if (sem_post(&addr_metadata->sem_file_request) == -1) {
            errExit("handle_with_cache: sem_post sem_file_request");
        }

        if (sem_wait(&addr_metadata->sem_file_ready) == -1) {
            errExit("handle_with_cache: sem_wait sem_file_ready");
        }

        memcpy(buffer, &shm_data_segment->buffer, addr_metadata->bytes_transferred);

        write_len = gfs_send(ctx, buffer, addr_metadata->bytes_transferred);
        if (write_len != addr_metadata->bytes_transferred) {
            errExit("handle_with_cache: gfs_send addr_metadata");
        }

        bytes_transferred += write_len;
    }

    cleanup_server_request(sqi, addr_metadata);

    return bytes_transferred;
}
