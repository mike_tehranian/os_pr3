#ifndef _SHM_CHANNEL_H_
#define _SHM_CHANNEL_H_


#define MAX_CACHE_REQUEST_LEN 6200
#define MQ_PATH "/mq_proxy_cache"

#define SM_DATA_PATH "/part2_data_segment_%d"
#define SM_META_PATH "/part2_meta_segment_%d"

typedef struct {
    char *data_name;
    char *meta_name;
} segment_queue_item;

typedef struct {
    ssize_t file_len;
    ssize_t bytes_transferred;
    sem_t sem_file_request;
    sem_t sem_file_ready;
} shared_mem_metadata;

typedef struct {
    char *buffer;
} shared_mem_data;

#endif
