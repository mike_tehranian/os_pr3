#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <printf.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/signal.h>
#include <unistd.h>
#include <curl/curl.h>

#include <pthread.h>
#include <mqueue.h>
#include <semaphore.h>
#include <sys/fcntl.h>
#include <sys/mman.h>

#include "cache-student.h"
#include "gfserver.h"
#include "shm_channel.h"
#include "simplecache.h"


#if !defined(CACHE_FAILURE)
#define CACHE_FAILURE (-1)
#endif // CACHE_FAILURE


mqd_t mqd;

void errExit(const char *msg) {
    perror(msg);
    exit(SERVER_FAILURE);
}

void* process_request(void *arg) {
    int fildes, fd;
    char message[MAX_CACHE_REQUEST_LEN];
    char shm_name_data[MAX_CACHE_REQUEST_LEN];
    char shm_name_meta[MAX_CACHE_REQUEST_LEN];
    size_t sm_data_segment_size, bytes_transferred, read_file_len;
    char path[MAX_CACHE_REQUEST_LEN];
    ssize_t num_read, write_len;

    for (;;) {
        memset(message, 0, MAX_CACHE_REQUEST_LEN);
        bytes_transferred = 0;

        num_read = mq_receive(mqd, message, MAX_CACHE_REQUEST_LEN, NULL);
        if (num_read == -1) {
            errExit("process_request: mq_receive");
        }

        sscanf(message, "%zu %s %s %s", &sm_data_segment_size,
                shm_name_data, shm_name_meta, path);

        fd = shm_open(shm_name_meta, O_RDWR, 0);
        if (fd == -1) {
            errExit("process_request: shm_open meta");
        }

        shared_mem_metadata *addr_metadata;
        addr_metadata = mmap(NULL, sizeof(shared_mem_metadata), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr_metadata == MAP_FAILED) {
            errExit("process_request: mmap meta");
        }
        // LPI - Once we have mapped the object, we can close the file descriptor without
        // affecting the (memory) mapping.
        if (close(fd) == -1) {
            errExit("process_request: close fd");
        }

        fildes = simplecache_get(path);
        // Simple Cache returns -1 if file path does not exist
        if (fildes == -1) {
            // Send error back to Proxy that file was not found
            addr_metadata->file_len = -1;
            if (sem_post(&addr_metadata->sem_file_ready) == -1) {
                errExit("process_request: sem_post sem_file_ready");
            }
            // Process the next MQ request
            continue;
        }

        addr_metadata->file_len = lseek(fildes, 0, SEEK_END);
        lseek(fildes, 0, SEEK_SET);

        // Notify Proxy on file len
        if (sem_post(&addr_metadata->sem_file_ready) == -1) {
            errExit("process_request: sem_post sem_file_ready 2");
        }

        fd = shm_open(shm_name_data, O_RDWR, 0);
        if (fd == -1) {
            errExit("process_request: shm_open fd");
        }

        shared_mem_data *addr;
        addr = mmap(NULL, sm_data_segment_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
        if (addr == MAP_FAILED) {
            errExit("process_request: mmap data");
        }
        if (close(fd) == -1) {
            errExit("process_request: close fd data");
        }

        char file_contents_buffer[sm_data_segment_size];
        while (bytes_transferred < addr_metadata->file_len) {
            if (sem_wait(&addr_metadata->sem_file_request) == -1) {
                errExit("process_request: sem_wait file_request");
            }

            read_file_len = addr_metadata->file_len - bytes_transferred;
            // Can only request and populate the amount of bytes available
            // in the SM segment
            if (read_file_len > sm_data_segment_size) {
                read_file_len = sm_data_segment_size;
            }

            write_len = read(fildes, file_contents_buffer, read_file_len);
            if (write_len <= 0) {
                errExit("process_request: write_len");
            }

            memcpy(&addr->buffer, file_contents_buffer, write_len);

            addr_metadata->bytes_transferred = write_len;

            if (sem_post(&addr_metadata->sem_file_ready) == -1) {
                errExit("process_request: sem_post file_ready");
            }

            bytes_transferred += write_len;
        }
    }

    pthread_exit(NULL);
}

static void _sig_handler(int signo) {
    if (signo == SIGINT || signo == SIGTERM) {
        /* Unlink IPC mechanisms here*/
        close(mqd);
        mq_unlink(MQ_PATH);

        exit(signo);
    }
}

#define USAGE                                                                 \
"usage:\n"                                                                    \
"  simplecached [options]\n"                                                  \
"options:\n"                                                                  \
"  -c [cachedir]       Path to static files (Default: ./)\n"                  \
"  -t [thread_count]   Thread count for work queue (Default: 3, Range: 1-1024)\n"      \
"  -h                  Show this help message\n"

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
  {"cachedir",           required_argument,      NULL,           'c'},
  {"nthreads",           required_argument,      NULL,           't'},
  {"help",               no_argument,            NULL,           'h'},
  {"hidden",             no_argument,            NULL,           'i'}, /* server side */
  {NULL,                 0,                      NULL,             0}
};

void Usage() {
  fprintf(stdout, "%s", USAGE);
}

int main(int argc, char **argv) {
    int nthreads = 3;
    char *cachedir = "locals.txt";
    char option_char;
    int status = 0;

    /* disable buffering to stdout */
    setbuf(stdout, NULL);

    while ((option_char = getopt_long(argc, argv, "ic:ht:x", gLongOptions, NULL)) != -1) {
        switch (option_char) {
            default:
                status = 1;
            case 'h': // help
                Usage();
                exit(status);
            case 'c': //cache directory
                cachedir = optarg;
                break;
            case 't': // thread-count
                nthreads = atoi(optarg);
                break;
            case 'i': // server side usage
                break;
            case 'x': // experimental
                break;
        }
    }

    if ((nthreads>6200) || (nthreads < 1)) {
        fprintf(stderr, "Invalid number of threads\n");
        exit(__LINE__);
    }

    if (SIG_ERR == signal(SIGINT, _sig_handler)){
        fprintf(stderr,"Unable to catch SIGINT...exiting.\n");
        exit(CACHE_FAILURE);
    }

    if (SIG_ERR == signal(SIGTERM, _sig_handler)) {
        fprintf(stderr,"Unable to catch SIGTERM...exiting.\n");
        exit(CACHE_FAILURE);
    }

    /* Cache initialization */
    simplecache_init(cachedir);

    /* Add your cache code here */
    do {
        // Check that the Proxy server has created the MQ every 3 seconds
        sleep(3);
        mqd = mq_open(MQ_PATH, O_RDONLY);
    } while (mqd == -1);

    int i = 0;
    pthread_t threads[nthreads];

    for (i = 0; i < nthreads; i++) {
        pthread_create(&threads[i], NULL, process_request, NULL);
    }

    for (i = 0; i < nthreads; i++) {
        pthread_join(threads[i], NULL);
    }

    /* this code probably won't execute */
    return 0;
}
