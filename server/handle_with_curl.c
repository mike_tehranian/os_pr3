#include <curl/curl.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <printf.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/signal.h>
#include <unistd.h>

#include "gfserver.h"
#include "proxy-student.h"

#define BUFSIZE (6200)

// The code in this file was heavily inspired from the referenced tutorial code
// from here: https://curl.haxx.se/libcurl/c/getinmemory.html

struct memory_struct {
  char *memory;
  size_t size;
};

static size_t write_memory_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct memory_struct *mem = (struct memory_struct *)userp;

    mem->memory = realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
      return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

/*
 * handle_with_curl is the suggested name for your curl call back.
 * We suggest you implement your code here.  Feel free to use any
 * other functions that you may need.
 */
ssize_t handle_with_curl(gfcontext_t *ctx, char *path, void* arg) {
    CURL *curl_handle;
    /* CURLcode res; */

    struct memory_struct chunk;
    chunk.memory = malloc(1);
    chunk.size = 0;

    // Create request URL
    char url_buffer[8192];
    strcpy(url_buffer, arg);
    strcat(url_buffer, path);

    ssize_t bytes_sent = 0;

    long http_response_code = 0;
    double cl = 0.0;

    curl_handle = curl_easy_init();
    curl_easy_setopt(curl_handle, CURLOPT_URL, url_buffer);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_memory_callback);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    /* res = curl_easy_perform(curl_handle); */
    curl_easy_perform(curl_handle);

    curl_easy_getinfo(curl_handle, CURLINFO_RESPONSE_CODE, &http_response_code);
    curl_easy_getinfo(curl_handle, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &cl);

    if (http_response_code == 200 && cl > 0.0) {
        gfs_sendheader(ctx, GF_OK, chunk.size);
        bytes_sent = gfs_send(ctx, chunk.memory, chunk.size);

        curl_easy_cleanup(curl_handle);
        free(chunk.memory);
        return bytes_sent;
    } else if (http_response_code == 404 || http_response_code == 403) {
        curl_easy_cleanup(curl_handle);
        free(chunk.memory);
        return gfs_sendheader(ctx, GF_FILE_NOT_FOUND, 0);
    } else {
        gfs_sendheader(ctx, GF_ERROR, 0);
        curl_easy_cleanup(curl_handle);
        free(chunk.memory);
    }

    curl_easy_cleanup(curl_handle);
    free(chunk.memory);
    return bytes_sent;
}


/*
 * The original sample uses handle_with_file.  We add a wrapper here so that
 * you can use this in place of the existing invocation of handle_with_file.
 *
 * Feel free to use handle_with_curl directly.
 */
ssize_t handle_with_file(gfcontext_t *ctx, char *path, void* arg) {
    return handle_with_curl(ctx, path, arg);
}
