# Project README file

## How I Implemented my Functionality
For Part 1, my implementation was based highly on the sample code provided from the linked resources on the project description page: https://curl.haxx.se/libcurl/c/getinmemory.html and https://www.hackthissite.org/articles/read/1078
I started by reading all of the information on the following site: https://curl.haxx.se/libcurl/c to get a high-level overview of all of the components involved in making a CURL request. I used the easy interface for CURL because I wanted to use blocking CURL commands. Additionally, the calling webproxy server itself is multi-threaded so it is okay to make each of the worker threads perform blocking synchronous CURL calls without blocking the server from serving other requests. I learned how the init and cleanup calls for the curl handlers are not thread-safe and should be performed in the main thread back in the webproxy launcher. Using the resources listed above and in the project description was sufficient to implement this part.

For Part 2, I first learned about message queues, semaphore, and shared memory from Beej's Guide to Unix IPC. I knew that I had to incorporate these concepts together for this part so I looked for further code examples on the internet. Unfortunately, the resources that I found online for the System V API had a wide range of quality and it was a little tricky to tie all of the concepts together and how I would use them to solve part 2. I then followed a suggestion that I read about on an old Piazza post about reading the book "The Linux Programming Interface" (LPI). I purchased the e-book and read chapters 51-54 and was very impressed with the presentation and code examples. Specifically Example 53-6 from the book solidified in my head how to solve Part 2. This example ties together concepts of multithreading and access to a shared variable using semaphores. For Part 2, all I had to do was replace that shared variable in the example with a shared buffer (and some meta-data about the buffer) and my problem would be solved. And that is essentially what I did for Part 2 of this assignment.
As the problem description suggested I used message queues for the Command Channel for sending requests between the Proxy and the Cache. This was fairly straightforward to implement and I again leveraged the LPI book on best practices for implementing message queues.
Finally, the last tricky part of Part 2 was maintaining a collection of used and free shared memory segments. Originally I had made an array with a struct of a boolean flag and the metadata saying whether a Shared memory segment was available and its name. However this is an inefficient implementation as searching through the array for free segments is an O(n) implementation. I then noticed that the project description recommended using <steque.h> for the shared memory segments and then it became clear that the optimal implementation should maintain a queue of the free shared memory segments in a queue and shared memory segments should be enqueued and popped off as they become free and used respectively.

## How I Tested My Components
For Part 1, I tested my webproxy and CURL handler locally by downloading all of the resources from the AWS S3 bucket into my local vagrant instance. I initially failed some tests on Bonnie because I forgot to remove the hard-coded AWS S3 URL from my webproxy.c. I also wasn't checking for 404s and non-existent files properly in my CURL handler so I had to resubmit a few times. I added more checks for the HTTP responses code from the server and sent back the appropriate response to the GFClient based upon the files existence.

For the Part 1 grader, I would have added tests to the test suite which check for 300-range HTTP status codes for Part 1. For example a test which tested a resource with a 302 (Redirect) status code, and making sure that the GFClient received the appropriate status back from the WebProxy.

For Part 2, in order to test I first created the proxy server and then the cache server. Then, I created a few batch scripts that ran through multiple test scenarios:
* Invalid or non-existent file requested by the GFClient workload
* Multiple GFCLients making requests to the WebProxy and thus the Cache at the same time.
* File requests for files which exceed the shared memory segment size
* Requests which failed during the transfer of multiple hops between the Proxy and Server. I did this by having file sizes which exceed the shared memory segment size and then failing after the first data transfer from the Cache to the Proxy.
* Workload sizes which exceed the number of shared memory segments initialized by the Proxy
I also saw the following  Piazza post where another student utilized similar ideas for testing for the Proxy and Cache data transfer: https://piazza.com/class/jc6jlz153n3462?cid=926

For the Part 2 grader, I would add tests for all of the test cases above to the grader test suite.

## Challenges Overcame
For Part 1, the challenges were to read through all of the provided CURL documentation and code examples and making sure that the right responses were sent back to the GFClient depending on the status of the file requested. Part 1 was not particularly difficult as I mentioned earlier because I used a lot of the sample code which was referenced from the project description and Piazza helpful links posts.

For Part 2, there were many challenges for which I overcame. It was hard taking the concepts of Shared Memory, Message Queues, and Semaphores together and come up with the right design to solve the problem. The biggest resource that helped me was the book: "The Linux Programming Interface" Chapters 51-54. I originally tried using the System V APIs but I found the required extra use of an extra file descriptor from ftok() calls to be cumbersome and it was hard to find consistently good quality code examples for the System V IPC API. I spent several days reading through Beej's guide to Unix IPC but ultimately I went with a different approach as I used the POSIX API and highly leveraged the resource "The Linux Programming Interface" instead.

Example 53-6 from "The Linux Programming Interface" really solidified in my head how the Proxy and Server would communicate and synchronize with each other and I ended up using a design derived from that example for synchronizing the shared memory segments between the Proxy and Server correctly.

## Known Bugs/Issues/Limitations
* For Part 1, my code passes all of the tests so I don't have any known bugs there. Some known limitations is that I don't check for every type of HTTP Responses status code which is possible to be returned back from the server. Specifically I don't check for 300-range HTTP status codes for redirects, so my handler could be more robust to handler HTTP resources which have been moved. Also, there are some known limitations with buffer sizes used to fetch data within the CURL handlers. Specifically, I set a (relatively high) URL Buffer size of 8192 bytes which seems large enough for me, but could have limitations if there is a longer URL desired. Fun fact, only Google Chrome supports URLs of length greater than 8192 characters: https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers

For Part 2, my codes passes all of the tests on Bonnie so I don't have any known bugs there. Some known limitations is that given that there is a fixed number of memory segments then there can only be a maximum number of concurrent requests handled by the Proxy server at a given time. Also, another limitation is that all shared memory segments have the same fixed size. This can lead to unused shared memory space and thus internal fragmentation of the shared memory segments. If there was a lookup that contained the size of each of the served files then the proxy could create a shared memory segment sufficiently only enough to store the buffer contents of that file and not more. Furthermore, if a file's size is larger than a memory segment then this can lead to more overhead as multiple round trip communications between the proxy and cache must occur. Therefore having appropriately sized shared memory segments for each file is very important for optimal performance.

## If I were to do this project again

If I were going to do this project again, I would skip learning about the System V API from Beej's guide to Unix IPC and gone straight to learning the POSIX API and examples found in the book "The Linux Programming Interface". In fact I think the course instructors should highlight that book and the relevant chapters directly in the project resources in the description. There was very little information provided on the project description on how to approach Part 2 and it was hard finding good quality resources online. The LPI book was a life saver for me and explained the concepts efficiently and also provided examples like Example 53-6 which was similar to Part 2. By reading how multiple threads can mutate (shared) memory using the post()/wait() pattern, my approach for Part 2 became much more clear.

## References

* Chapters 51-54 from Michael Kerrisk: “The Linux Programming Interface: A Linux and UNIX® System Programming Handbook.”
* https://users.cs.cf.ac.uk/Dave.Marshall/C/node25.html
* http://beej.us/guide/bgipc/html/multi/index.html
* https://www.thegeekstuff.com/2012/03/linux-signals-fundamentals/
* https://www.thegeekstuff.com/2012/03/catch-signals-sample-c-code/
* https://www.hackthissite.org/articles/read/1078
* https://curl.haxx.se/libcurl/c/
* https://daniel.haxx.se/blog/2018/03/14/heres-curl-7-59-0/
* https://www.tutorialspoint.com/c_standard_library/c_function_strncat.htm
* https://www.geeksforgeeks.org/posix-shared-memory-api/
* http://pubs.opengroup.org/onlinepubs/009604599/functions/shm_open.html
* https://stackoverflow.com/questions/4582968/system-v-ipc-vs-posix-ipc
* https://curl.haxx.se/libcurl/c/CURLINFO_CONTENT_LENGTH_DOWNLOAD.html
* http://man7.org/conf/lca2013/IPC_Overview-LCA-2013-printable.pdf
* https://curl.haxx.se/libcurl/c/getinmemory.html
* https://stackoverflow.com/questions/5701587/can-someone-explain-the-arguments-to-write-function-used-for-the-curl-option-cur
* https://stackoverflow.com/questions/48969114/meaning-of-pathname-using-ftok
* https://forums.aws.amazon.com/thread.jspa?threadID=56531
* https://curl.haxx.se/libcurl/c/curl_easy_getinfo.html
* https://curl.haxx.se/libcurl/c/curl_easy_setopt.html
* https://stackoverflow.com/questions/7677285/how-to-get-the-length-of-a-file-without-downloading-the-file-in-a-curl-binary-ge
* https://www.thomas-krenn.com/en/wiki/Linux_Page_Cache_Basics
* http://www.it.uom.gr/teaching/c_marshall/node25.html
* http://pubs.opengroup.org/onlinepubs/009695399/functions/fstat.html
* http://man7.org/linux/man-pages/man2/pread.2.html
* https://stackoverflow.com/questions/6537436/how-do-you-get-file-size-by-fd
* https://www.softprayog.in/programming/interprocess-communication-using-posix-message-queues-in-linux
* http://menehune.opt.wfu.edu/Kokua/More_SGI/007-2478-008/sgi_html/ch06.html
* http://pubs.opengroup.org/onlinepubs/7908799/xsh/mqueue.h.html
* https://stackoverflow.com/questions/3056307/how-do-i-use-mqueue-in-a-c-program-on-a-linux-based-system
* http://pubs.opengroup.org/onlinepubs/007904875/basedefs/sys/mman.h.html
* http://www.cse.psu.edu/~deh25/cmpsc473/notes/OSC/Processes/shm.html
* http://www.cse.psu.edu/~deh25/cmpsc473/notes/OSC/Processes/shm-posix-producer-orig.c
* http://www.cse.psu.edu/~deh25/cmpsc473/notes/OSC/Processes/shm-posix-consumer-orig.c
* http://pubs.opengroup.org/onlinepubs/009696799/basedefs/mqueue.h.html
* https://macboypro.wordpress.com/2009/05/15/posix-message-passing-in-linux/
